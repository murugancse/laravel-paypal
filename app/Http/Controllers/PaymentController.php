<?php

namespace App\Http\Controllers;

use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;

class PaymentController extends Controller
{
	 public $apiContext;

	 public function __construct()
	 {

	 }
	 public function create(){
		  $apiContext = new \PayPal\Rest\ApiContext(
					new \PayPal\Auth\OAuthTokenCredential(
						'AfnBXULVJM0pyvzJNj24fyIg-gy4JSwbRLL0eZ1_RVzgteGVYj7NHp4a_PaSle4jirCYe_Bvt4g-GDAi',     // ClientID
						'EIvdeQuRkRUrF3aD1ghCSnHXvf2Ggex5BQGcg_Z6K9MqRKe7fh-PFrI8KyXBJulTf33554Fp5ET-78XB'      // ClientSecret
					)
			);		

			$payer = new Payer();
			$payer->setPaymentMethod("paypal");

			$item1 = new Item();
			$item1->setName('Ground Coffee 40 oz')
					->setCurrency('USD')
					->setQuantity(1)
					->setSku("123123") // Similar to `item_number` in Classic API
					->setPrice(7.5);
			$item2 = new Item();
			$item2->setName('Granola bars')
					->setCurrency('USD')
					->setQuantity(5)
					->setSku("321321") // Similar to `item_number` in Classic API
					->setPrice(2);
			
			$itemList = new ItemList();
			$itemList->setItems(array($item1, $item2));

			$details = new Details();
			$details->setShipping(1.2)
					->setTax(1.3)
					->setSubtotal(17.50);
			
			$amount = new Amount();
			$amount->setCurrency("USD")
					->setTotal(20)
					->setDetails($details);
			
			$transaction = new Transaction();
			$transaction->setAmount($amount)
					->setItemList($itemList)
					->setDescription("Payment description")
					->setInvoiceNumber(uniqid());
			
			$redirectUrls = new RedirectUrls();
			$redirectUrls->setReturnUrl("http://localhost/repository/task/task1/paypal/public/execute-payment")
						->setCancelUrl("http://localhost/repository/task/task1/paypal/public/execute-payment");
			
			$payment = new Payment();
			$payment->setIntent("sale")
					->setPayer($payer)
					->setRedirectUrls($redirectUrls)
					->setTransactions(array($transaction));
			
			$payment->create($apiContext);

			$approvalUrl = $payment->getApprovalLink();
			//return $payment->getApprovalLink();
			return redirect($approvalUrl);
		//	echo '44';
	 }

    public function execute(){
		// After Step 1
	  	$apiContext = new \PayPal\Rest\ApiContext(
					new \PayPal\Auth\OAuthTokenCredential(
						'AfnBXULVJM0pyvzJNj24fyIg-gy4JSwbRLL0eZ1_RVzgteGVYj7NHp4a_PaSle4jirCYe_Bvt4g-GDAi',     // ClientID
						'EIvdeQuRkRUrF3aD1ghCSnHXvf2Ggex5BQGcg_Z6K9MqRKe7fh-PFrI8KyXBJulTf33554Fp5ET-78XB'      // ClientSecret
					)
			);	
			
			$paymentId = request('paymentId');
			$payment = Payment::get($paymentId, $apiContext);

			$execution = new PaymentExecution();
			$execution->setPayerId(request('PayerID'));
			
			$transaction = new Transaction();
			$amount = new Amount();
			$details = new Details();
			
			$details->setShipping(1.2)
					->setTax(1.3)
					->setSubtotal(17.50);
			
			$amount->setCurrency('USD');
			$amount->setTotal(20);
			$amount->setDetails($details);
			$transaction->setAmount($amount);
			
			$execution->addTransaction($transaction);
			//exit;
			try {

				$result = $payment->execute($execution, $apiContext);
				//print_r(array("Executed Payment", "Payment", $payment->getId(), $execution, $result));

				try {
					$payment = Payment::get($paymentId, $apiContext);
				} catch (Exception $ex) {

					print_r(array("Get Payment", "Payment", null, null, $ex));
					exit(1);
				}
			} catch (Exception $ex) {

				print_r(array("Executed Payment", "Payment", null, null, $ex));
				exit(1);
			}

			//print_r(array("Get Payment", "Payment", $payment->getId(), null, $payment));

			return $payment;

	}
}
